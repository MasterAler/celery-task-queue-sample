# -*- coding: utf-8 -*-

from celery_tasks import gen_prime

samples = [100, 200, 300]

for i in samples:
	gen_prime.delay(i)
