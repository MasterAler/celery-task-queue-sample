# -*- coding: utf-8 -*-

import sys
import os
from celery_tasks import start_rzline_calculations

#CALC_DIR = '/home/public/celery_queue/calc_cases'
CALC_DIR = '/home/khadfedor/rzline/'

subdirs = next(os.walk(CALC_DIR))[1]
for subdir in subdirs:
    cases = next(os.walk(os.path.join(CALC_DIR, subdir)))[1]
    for case in cases:
        start_rzline_calculations.delay(os.path.join(CALC_DIR, subdir, case))

