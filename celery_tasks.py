# -*- coding: utf-8 -*-

from celery import Celery
import sys 
import os
import shlex
import shutil
import subprocess
from datetime import datetime

app = Celery('rzline', backend='amqp', broker='amqp://')
app.conf.CELERY_TASK_SERIALIZER = 'json'
app.conf.CELERY_ACCEPT_CONTENT = ['json', 'msgpack', 'yaml']

LOG_FILENAME = "CELERY_QUEUE.log"

@app.task(ignore_result=True)
def print_hello():
    print('hello there')

@app.task
def gen_prime(x):
    f = open(LOG_FILENAME, "a", encoding="utf-8")
    multiples = []
    results = []
    for i in range(2, x+1):
        if i not in multiples:
            results.append(i)
            for j in range(i*i, x+1, i):
                multiples.append(j)
    f.write("%s\t%s" % (datetime.now().strftime("%I:%M%p on %B %d, %Y"), x))
    f.close()
    return results

@app.task(ignore_result=True)
def write_files():
    f = open("/home/public/celery_queue/test.txt", "w")
    f.write("FUUUU")
    f.close()

@app.task#(ignore_result=True)
def start_rzline_calculations(path):
    f = open(LOG_FILENAME, "a", encoding="utf-8")
    out_dir = os.path.join(path, 'Output')
    if os.path.exists(out_dir):
        shutil.rmtree(out_dir)

    command = 'wine RZLINE.exe'
    subprocess_handle = subprocess.Popen(shlex.split(command), cwd=path, shell=False, stdout=subprocess.PIPE)
    #subprocess_handle.wait()
    result = subprocess_handle.communicate()[0]
    f.write("%s\t%s" % (datetime.now().strftime("%I:%M%p on %B %d, %Y"), path))
    f.close()
    return "done: task " + path + " finished and shitted " + str(len(result)) + " symbols into output"
